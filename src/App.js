import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import './App.css';
import Navbar from './components/Navbar';
import Loader from './components/layout/Loader';
import Error from './components/layout/Error';
import Products from './components/Products';
import Footer from './components/Footer';
import NoProductsFound from './components/layout/NoProductsFound';
import AddNewProduct from './components/AddNewProduct';
import axios from 'axios';
import UpdateProduct from './components/UpdateProduct';

class App extends React.Component {

  constructor(props) {
    super(props);

    this.API_STATES = {
      LOADING: "loading",
      LOADED: "loaded",
      ERROR: "error",
    };

    this.state = {
      products: [],
      product: {},
      status: this.API_STATES.LOADING,
      errorMessage: "",
    };

    this.URL = 'https://fakestoreapi.com/products';
  }

  fetchData = (url) => {
    this.setState({
      status: this.API_STATES.LOADING,
    }, () => {
      axios.get(url)
        .then((response) => {
          this.setState({
            products: response.data,
            status: this.API_STATES.LOADED
          });
        })
        .catch((err) => {
          this.setState({
            status: this.API_STATES.ERROR,
            errorMessage: "An API error occurred. Please try again in a few minutes."
          });
        });
    });
  }

  generateNewId = () => {
    const newId = this.state.products
      .reduce((maxIdvalue, product) => {
        if (product.id > maxIdvalue) {
          maxIdvalue = product.id;
        }
        return maxIdvalue;
      }, 0) + 1;

    return newId;
  };

  addProduct = (title, category, description, price, image) => {

    const newProduct = {};

    newProduct.id = this.generateNewId();
    newProduct.title = title;
    newProduct.price = price;
    newProduct.description = description;
    newProduct.category = category;
    newProduct.image = image;
    newProduct.rating = {
      rate: 0,
      count: 0
    };

    const newProducts = this.state.products;
    newProducts.push(newProduct);

    this.setState({
      products: newProducts
    });
  }

  updateProduct = (title, category, description, price, image, productID) => {

    const updatedProducts = this.state.products;

    const updatedProduct = updatedProducts.find((product) => {
      return product.id === productID;
    });

    updatedProduct.title = (title !== '') ? title : updatedProduct.title;

    updatedProduct.category = (category !== '') ? category : updatedProduct.category;
    updatedProduct.description = (description !== '') ? description : updatedProduct.description;
    updatedProduct.description = description;
    updatedProduct.price = (price !== '') ? price : updatedProduct.price;
    updatedProduct.image = (image !== '') ? image : updatedProduct.image;

    this.setState({
      products: updatedProducts
    });

  }

  deletePopUp = (product) => {
    this.setState({
      product,
    });

    this.deleteProduct(product);
  }

  deleteProduct = (product) => {
    const filteredProduct = this.state.products
      .filter((currentProduct) => {
        return currentProduct.id !== product.id;
      });

    this.setState({
      products: filteredProduct,
    })
  }

  componentDidMount() {
    console.log("fetch")
    this.fetchData(this.URL);
  }

  render() {

    return (
      <Router>
        <div className='App'>
          <Navbar />

          {this.state.status === this.API_STATES.LOADING && <Loader />}
          {this.state.status === this.API_STATES.ERROR && <Error errorMessage={this.state.errorMessage} />}
          {this.state.status === this.API_STATES.LOADED && this.state.products.length === 0 && <NoProductsFound />}
          {
            this.state.status === this.API_STATES.LOADED && this.state.products.length > 0 &&
            <Routes>
              <Route path="/" element={<Products products={this.state.products} deletePopUp={this.deletePopUp} />} />
              <Route path="/add-product" element={<AddNewProduct addProduct={this.addProduct} />} />
              <Route path="/update-product/:id" element={<UpdateProduct products={this.state.products} updateProduct={this.updateProduct} />} />
            </Routes>
          }

          <Footer />
        </div>
      </Router>
    );
  }
}

export default App;