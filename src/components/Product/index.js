import React from 'react';

import './Product.css';
import ProductDescription from '../layout/ProductDescription';
import { Link } from 'react-router-dom';

class Product extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            showMenu: false,
        };
    }

    handleClickMenu = () => {
        this.setState((currentState) => {
            return {
                showMenu: !currentState.showMenu,
            }
        });

        setTimeout(()=> {
            this.setState((currentState) => {
                return {
                    showMenu: !currentState.showMenu,
                }
            });
        }, 3000);
    }

    handleClickDelete = () => {
        this.props.deletePopUp(this.props.product);
    }

    render() {
        return (
            <div className='product'>
                <img
                    className='productImage'
                    src={this.props.product.image}
                    alt=''
                />
                <span className='options'
                    onClick={this.handleClickMenu}>...</span>

                {this.state.showMenu &&
                    <div className='popup-menu'>
                        <span id="edit-product"><Link to={`update-product/${this.props.product.id}`} style={{ color: '#000', textDecoration: 'none' }}>edit product</Link></span>
                        <span id="delete-product" onClick={this.handleClickDelete}>delete product</span>
                    </div>
                }

                <ProductDescription title={this.props.product.title} rating={this.props.product.rating} />

                <div className='priceContainer'>
                    <h4>${this.props.product.price}</h4>
                    <Link to='/'>
                        <i className='fal fa-shopping-cart cart'></i>
                    </Link>
                </div>
            </div>
        );
    }
}

export default Product;