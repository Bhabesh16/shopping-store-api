import React from 'react';
import { Link } from 'react-router-dom';

import './Footer.css';

export class Footer extends React.Component {
    render() {
        return (
            <div className="footer">
                <Link to="/">
                    <img
                        src='https://www.svgrepo.com/show/217771/shopping-logo.svg'
                        className='logo'
                        alt=''
                    />
                </Link>
                <div className='copyright'>
                    © Shopping Store. All Rights Reserved
                </div>
            </div>
        );
    }
}

export default Footer;