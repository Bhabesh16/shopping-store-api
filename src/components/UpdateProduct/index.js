import React from 'react';

import ProductForm from '../ProductForm';

class UpdateProduct extends React.Component {

    id = Number(window.location.href.split('/').slice(4));

    getProduct = () => {
        const product = this.props.products.find((currentProduct) => {
            return currentProduct.id === this.id;
        });

       return product;
    }


    render() {

        return (
            <ProductForm headTitle="UPDATE PRODUCT" product={this.getProduct()} updateProduct={this.props.updateProduct} />
        );
    }
}

export default UpdateProduct;