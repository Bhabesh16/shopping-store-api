import React from 'react';
import { Link } from 'react-router-dom';

import './NavRight.css';

class NavRight extends React.Component {
    render() {
        return (
            <div className="navRight">
                <ul className="navRightContainer">
                    <li><Link to="/add-product" id="addNewProduct">ADD NEW PRODUCT</Link></li>
                </ul>
            </div>
        );
    }
}

export default NavRight;