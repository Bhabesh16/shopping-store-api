import React from 'react';

import './NoProductsFound.css';
import noProduct from '../../../assets/images/no-product.png';

class NoProductsFound extends React.Component {
    render() {
        return (
            <div className="no-products-container">
                <img
                    className='no-products-img'
                    src={noProduct}
                    alt=''
                />
                <div className="no-products-text-container">
                    <span className="no-products-heading">No Products Found</span>
                    <div>
                        <div className="no-products-message">No products available at the moment. Please try again later.</div>
                    </div>
                </div>
            </div>
        );
    }
}

export default NoProductsFound;