import React from 'react'
import { Link } from 'react-router-dom';

import './NavMiddle.css';

class NavMiddle extends React.Component {

    render() {
        return (
            <div className="navMiddle">
                <ul id="navbar" className={!this.props.mobileNav ? 'hide' : ''}>
                    <li><Link to="/" className="active nav-ele">Home</Link></li>
                    <li><Link to="/" className="nav-ele">Shop</Link></li>
                    <li><Link to="/" className="nav-ele">About</Link></li>
                    <li><Link to="/" className="nav-ele">Contact</Link></li>
                    <li><Link to="/" className="nav-ele"><i className="far fa-shopping-bag"></i></Link></li>
                    <ul className="navRightContainer" id="mobileRight">
                        <li style={{ fontSize: '1em', width: '12em', padding: '0' }}>
                            <Link to="/add-product" id="addNewProduct">ADD NEW PRODUCT</Link>
                        </li>
                    </ul>
                </ul>
            </div>
        );
    }
}

export default NavMiddle;