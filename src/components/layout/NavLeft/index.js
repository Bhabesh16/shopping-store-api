import React from 'react';
import { Link } from 'react-router-dom';

import './NavLeft.css';

class NavLeft extends React.Component {
    render() {
        return (
            <div className='navLeft'>
                <Link to="/">
                    <img
                        src="https://www.svgrepo.com/show/217771/shopping-logo.svg"
                        className="logo"
                        alt=""
                    />
                </Link>
            </div>
        );
    }
}

export default NavLeft;