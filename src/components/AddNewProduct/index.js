import React from 'react';
import ProductForm from '../ProductForm';

export class AddNewProduct extends React.Component {
    render() {
        return (
            <ProductForm headTitle="ADD NEW PRODUCT" addProduct={this.props.addProduct} />
        );
    }
}

export default AddNewProduct;